/**
 * Directive that will provide a text box for input.
 * Anything inserted in the text box will be reversed for yoda-speak.
 */
'use strict';

module.exports = function(ngModule) {
  require('./yoda-speak.service')(ngModule);
  require('./yoda-speak.less');

  ngModule.directive('yodaSpeak', function() {
    return {
      restrict: 'E',
      scope: {},
      template: require('./yoda-speak.html'),
      controllerAs: 'vm',
      controller: [
        '$scope',
        'yodaSpeak',
        function($scope, yodaSpeak) {
          let vm = this;
          vm.isTranslating = false;

          vm.translate = function(sentence) {
            vm.isTranslating = true;
            yodaSpeak.translate(sentence)
              .then(
                sentenceTranslated => vm.sentenceTranslated = sentenceTranslated,
                error => alert(error.message)
              ).finally(
                () => vm.isTranslating = false
              );
          }
        }
      ]
    };
  });
};
