/**
 * Service that connects with Yoda API in
 *   https://market.mashape.com/ismaelc/yoda-speak
 */
'use strict';

module.exports = function(ngModule) {
  ngModule.factory('yodaSpeak', [
    '$http',
    '$q',
    function($http, $q) {
      let _apiKey = null;

      return {
        setKey,
        translate
      };

      function setKey(key) {
        _apiKey = key;
      }

      function translate(sentence) {
        let deferred = $q.defer();

        if (_apiKey === null) {
          let errorMessage = 'Mashape API key is not set yet, aborting request...';
          console.error(errorMessage);
          deferred.reject({ message: errorMessage });
        } else {
          sentence = sentence || '';
          $http({
            method: 'GET',
            url: 'https://yoda.p.mashape.com/yoda',
            headers: {
              'X-Mashape-Key': _apiKey
            },
            params: { sentence }
          }).then(function(response) {
            deferred.resolve(response.data);
          }, function(error) {
            deferred.reject(error);
          });
        }

        return deferred.promise;
      }
    }
  ]);
};
