/**
 * The module that aggregates all directive modules.
 */
'use strict';

let directives = require.context('.', true, /\.main\.js?$/);

// Initialize the main directive module
let ngModule = angular.module('Godzilla.Directives', []);

// Load all directive modules
directives.keys().forEach(key => directives(key)(ngModule));

module.exports = ngModule;
