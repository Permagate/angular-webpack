/**
 * The main entry point that aggregates all content controllers.
 */
'use strict';

let controllers = require.context('.', true, /-controller\.js?$/);

// Attach all controllers to the provided module
module.exports = function(ngModule) {
  controllers.keys().forEach(key => controllers(key)(ngModule));
};
