/**
 * The main angular module.
 */
'use strict';

let angular  = require('angular'),
  directives = require('./directives');

// Initialize module
let app = angular.module('Godzilla', [
  'Godzilla.Directives'
]).run([
  'yodaSpeak',
  function(yodaSpeak) {
    yodaSpeak.setKey('zRKAER7Vszmshx2tXVcTmeFIzR6fp1GP6T4jsnaE0goq0CoD73');
  }
]);

// Load contents
require('./contents')(app);

module.exports = app;
